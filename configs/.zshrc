# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Prompt is instantly ready for typing without the need of loading all plugins first.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Make antigen available.
source ~/.antigen/antigen.zsh

# Use Oh-My-ZSH.
antigen use oh-my-zsh

# Use bundles directly from Oh-My-ZSH.
antigen bundle git
antigen bundle ssh
antigen bundle safe-paste
antigen bundle docker
antigen bundle docker-compose
antigen bundle dircycle

# Use bundles from zsh-users project.
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zsh-users/zsh-completions

# Use external bundles for conda autocompletion.
antigen bundle esc/conda-zsh-completion

# User PowerLevel10k theme.
antigen theme romkatv/powerlevel10k

# Apply everything from above to zsh.
antigen apply

# Add Keybindings.
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down


# Initialize Conda.
__conda_setup="$($HOME/miniconda3/bin/conda 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "$HOME/miniconda3/etc/profile.d/conda.sh" ]; then
        . "$HOME/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="$HOME/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup

# disable annoying beep sound
unsetopt BEEP

# Change autosuggestion color.
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=60'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Load environment variables if exist
[[ -f $HOME/.environment ]] && . $HOME/.environment

# Personal conda aliases
alias ca="conda activate"
alias cq="conda deactivate"
alias ccr="conda create"
