# Installation

## External Requirements

### Git
I mean... Of course...

### ZSH
Install zsh shell via

```shell
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install zsh
```

### MiniConda
Download MiniConda [here](https://docs.conda.io/en/latest/miniconda.html)
and install into your homedirectory without `sudo` rights, i.e. into
`~/miniconda3`.

### VSCode
You can get VSCode [here](https://code.visualstudio.com/docs/setup/linux).
After the installation of this repo you need to install the VSCode extensions
`Python`, `Atom One Dark Theme` and `vscode-icons`

### Powerlevel10K Fonts
Install Fonts from this page:
[MesloLGS NF Fonts](https://github.com/romkatv/powerlevel10k#manual-font-installation)


## Installer
Clone this repository and get all submodules via
```
git submodule update --init --recursive
```
Simply run `./install` in the root directory of this repository.


# Configuration

## Powerlevel10K Theme
You can reconfigure the theme via
```shell
p10k configure
```

## Company specifc environment configuration.
Company specific stuff like our PyPi server can be added in the 
`.environment` file in the home directoy.
Since they typically contain username and password we won't put
them in the git repository.

## Global Gitignore
To enable the global gitignore on your maachine run the command
```
git config --global core.excludesfile ~/.config/git/.gitignore
```
